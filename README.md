# sin (Service INventory)

Tool for creating a multi-source service registry of available software.

## Setup

Run the tool in a Docker container in the detached mode with the command:
```
docker-compose up -d
```

To stop a running container, run the command:
```
docker-compose down
```
The container has one persistent volume for the database, so that data will be preserved outside the container. 
Thus, stopping the container will not impact the saved data. To stop the container without preserving any data, run instead:
```
docker-compose down -v
```

## Services
Once the container is running, the following services are available:
### Redis database

The database is running on port `6379` by default. Browsing the database using the RedisInsight UI is possible by visiting http://localhost:8001.

### API

The API is listening on port `5000` by default. Documentation for the available endpoints is hosted on http://localhost:5000/docs.

## Usage

To trigger the initial data population on an empty database the following command is used:
```
docker exec service-inventory python initial.py
``` 

To update the records in the registry use:
```
docker exec service-inventory python update.py
```

## Configuration
There are two environment variables that can be configured:
- CPE_RECORDS_COUNT - maximum number of records fetched from the CPE Dictionary,
- CPE_API_KEY - valid key for the CPE Dictionary API,
- CPE_UPDATE_DAYS - defines the number of days (90 by default) from now for which the CPE updated records will be queried.

The desired values can be passed to the container when executing scripts. For example:
```
docker exec --env CPE_RECORDS_COUNT=10000 --env CPE_API_KEY=validApiKey service-inventory python initial.py
```
and
```
docker exec --env CPE_UPDATE_DAYS=30 --env CPE_API_KEY=validApiKey service-inventory python update.py
```