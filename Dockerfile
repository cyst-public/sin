# This dockerfile is a modification of https://fastapi.tiangolo.com/deployment/docker/
FROM python:3.10-alpine3.16 as requirements

WORKDIR /tmp

RUN pip install poetry

COPY ./pyproject.toml /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

FROM python:3.10-alpine3.16 

WORKDIR /src

ENV PYTHONPATH "/src/"

ENV PYTHONUNBUFFERED 1 

EXPOSE 5000

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "5000"]

COPY --from=requirements /tmp/requirements.txt /src/requirements.txt

RUN pip install --no-cache-dir  --upgrade -r /src/requirements.txt

COPY ./sin /src/
