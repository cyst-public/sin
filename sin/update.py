from os import environ
from multiprocessing import Pool
from typing import Optional
from sources import debian, chocolatey, cpe_update
from common import tag_services, store_service
from api import get_service_by_cpe, update_service
from template import Service


def update(new_service: Service) -> Optional[str]:
    """
    Process new service data either by creating a new Service or
    by updating an existing record.
    """
    service = get_service_by_cpe(new_service["cpe"]["id"])

    if service is None:
        return store_service(new_service)

    new_service["pk"] = service["pk"]
    if update_service(service["pk"], new_service):
        return service["pk"]
    return None


def main():
    """
    Perform updates of records in the registry from all sources, including tagging.
    """
    api_key = environ.get("CPE_API_KEY", "")
    days = environ.get("CPE_UPDATE_DAYS", 120)

    services = cpe_update(int(days), api_key)

    service_ids = []
    with Pool() as p:
        service_ids = p.map(update, services)

    chocolatey()
    debian()
    tag_services(service_ids)


if __name__ == "__main__":
    main()
