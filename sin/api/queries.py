import os
import requests
from typing import Dict, Optional, List
from template import Service


API_URL = os.environ.get("API_URL", "http://localhost:5000")


def combine_fields(service, fields):
    """
    Combine the service fields with a new subset of changed fields.
    """
    for key, value in fields.items():
        if key == "refs":
            for ref_type, refs in value.items():
                service["refs"][ref_type] = list(set(service["refs"][ref_type] + refs))
        else:
            service[key] = value
    return service


def get_service_by_cpe(cpe_id: str) -> Optional[Service]:
    """
    Send the API request to get services by CPE.
    """
    r = requests.get(f"{API_URL}/services/cpes/{cpe_id}")

    if r.status_code != 200:
        return None
    return r.json()


def get_service(uuid: str) -> Optional[Service]:
    """
    Send the API request to get service by id.
    """
    r = requests.get(f"{API_URL}/services/services/{uuid}")

    if r.status_code != 200:
        return None
    return r.json()


def get_services_by_title(query: str) -> Optional[List[Service]]:
    """
    Send the API request to get services whose titles contain the desired query.
    """
    r = requests.get(f"{API_URL}/services/titles", params={"title": query})

    if r.status_code != 200:
        return None
    return r.json()


def create_service(service: Service) -> Optional[str]:
    """
    Send the API request to create a new service.
    """
    r = requests.post(f"{API_URL}/services/", json=service)

    if r.status_code != 201:
        print(r.json())
        return None
    service = r.json()
    return service["pk"]


def update_service(uuid: str, fields: Dict) -> bool:
    """
    Send the API request to update a Service by modifiying certain fields only.
    """
    service = get_service(uuid)

    if service is None:
        return False

    service = combine_fields(service, fields)

    r = requests.put(f"{API_URL}/services/{uuid}", json=service)

    return r.status_code == 200
