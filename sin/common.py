from multiprocessing import Pool
from typing import Optional
from sources import github_get_repo, get_description
from tags import Tagger
from api import create_service, combine_fields
from template import Service


def get_additional_metadata(service):
    """
    Get metadata about package from additional sources.
    """
    links = service["refs"].values()

    github_links = list(filter(lambda link: "github.com" in link, links))

    if github_links:
        return github_get_repo(github_links[0])

    if not service["refs"].get("product") or not service["refs"]["product"]:
        return None

    product_desc = ""

    for link in service["refs"]["product"]:
        desc = get_description(link)

        if desc is not None:
            product_desc += desc

    return {"description": product_desc} if product_desc else None


def store_service(service: Service) -> Optional[str]:
    """
    Create a Service with additional metadata.
    """
    if service is None:
        return None

    additional_data = get_additional_metadata(service)

    if additional_data is not None:
        service = combine_fields(service, additional_data)

    return create_service(service)


def tag_services(service_ids):
    """
    Add tags to all Services identified by the provided ids.
    """
    tagger = Tagger()

    with Pool() as p:
        p.map(tagger.add_tags, service_ids)
