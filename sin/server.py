from fastapi import FastAPI
from fastapi.responses import JSONResponse
from redis_om import Migrator
from redis_om.model import NotFoundError
from template import Service
from pydantic import BaseModel
from typing import List

app = FastAPI()


class Message(BaseModel):
    message: str


class ServiceRefs(BaseModel):
    pk: str
    refs: List[str]


@app.post("/services/", status_code=201, response_model=Service)
async def create_service(service: Service):
    return service.save()


@app.get(
    "/services/services/{pk}",
    status_code=200,
    response_model=Service,
    responses={404: {"model": Message, "description": "Not found"}},
)
async def get_service(pk: str):
    try:
        service = Service.get(pk)
        return service
    except NotFoundError:
        return JSONResponse(status_code=404, content={"message": "Service not found"})


@app.get(
    "/services/cpes/{id}",
    status_code=200,
    response_model=Service,
    responses={404: {"model": Message, "description": "Not found"}},
)
async def get_service_by_cpe(id: str):
    try:
        service = Service.find(Service.cpe.id == id).first()
        return service
    except NotFoundError:
        return JSONResponse(status_code=404, content={"message": "CPE not found"})


@app.put(
    "/services/{pk}",
    status_code=200,
    responses={404: {"model": Message, "description": "Not found"}},
)
async def update_service(pk: str, service: Service):
    try:
        service.save()
    except NotFoundError:
        return JSONResponse(status_code=404, content={"message": "Service not found"})


@app.delete(
    "/services/{pk}",
    status_code=200,
    response_model=Message,
    responses={404: {"model": Message, "description": "Not found"}},
)
async def delete_service(pk: str):
    deleted = Service.delete(pk)
    if deleted == 1:
        return Message(message="Service deleted")
    return JSONResponse(status_code=404, content={"message": "Service not found"})


@app.get("/services/titles", status_code=200, response_model=List[Service])
async def search_services_by_title(title: str):
    return Service.find(Service.title << title).all()


@app.get("/services/tags", status_code=200, response_model=List[Service])
async def search_services_by_tags(tag: str):
    return Service.find(Service.tags << tag).all()


@app.on_event("startup")
async def startup():
    Migrator().run()
