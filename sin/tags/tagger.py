import os
import toml
import re
from typing import Optional
from api import update_service, get_service
from template import Service


class Tagger:
    """
    Add tags to Services.
    """

    def __init__(self) -> None:
        """
        Initialize the Tagger with the taglist.
        """
        path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.taglist = toml.load(f=f"{path}/taglist.toml")
        self.tag_matcher = {}

        for tag, keys in self.taglist["tags"].items():
            keywords = keys["keys"]
            keywords = list(map(lambda word: r"\b" + word + r"\b", keywords))
            self.tag_matcher[tag] = re.compile("|".join(keywords), re.IGNORECASE)

    def add_tags(self, service_pk: str) -> None:
        """
        Add tags to a Service based on its description or its title.
        """
        service: Optional[Service] = get_service(service_pk)

        if service is None:
            return

        description = service.get("description")
        tags = []

        if description is None:
            description = service["title"]

        for tag, regex in self.tag_matcher.items():
            if regex.match(description):
                tags.append(tag)

        if not tags:
            return

        update_service(service["pk"], {"tags": tags})
