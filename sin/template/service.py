from redis_om import EmbeddedJsonModel, Field, JsonModel
from typing import Optional, List
from datetime import datetime


class CPE(EmbeddedJsonModel):
    name: str = Field(index=True)
    id: str = Field(index=True)
    last_modified: datetime = Field(index=True)
    created: datetime = Field(index=True)


class References(EmbeddedJsonModel):
    advisory: Optional[List[str]] = Field(index=True, default=[])
    change_log: Optional[List[str]] = Field(index=True, default=[])
    product: Optional[List[str]] = Field(index=True, default=[])
    project: Optional[List[str]] = Field(index=True, default=[])
    vendor: Optional[List[str]] = Field(index=True, default=[])
    version: Optional[List[str]] = Field(index=True, default=[])
    source: Optional[List[str]] = Field(index=True, default=[])
    misc: Optional[List[str]] = Field(index=True, default=[])


class Service(JsonModel):
    cpe: CPE
    title: str = Field(index=True, full_text_search=True)
    refs: References
    vendor: str = Field(index=True)
    product: str = Field(index=True, full_text_search=True)
    version: str
    target_sw: str = Field(index=True)
    downloads: Optional[int] = Field(index=True)
    description: Optional[str] = Field(index=True, full_text_search=True)
    tags: Optional[List[str]] = Field(index=True)
