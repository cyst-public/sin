from os import environ
from multiprocessing import Pool
from sources import debian, chocolatey, cpe_load
from common import tag_services, store_service


def main() -> None:
    """
    Perform the initial data population of the registry, including fetching data
    from all sources and tagging.
    """
    api_key = environ.get("CPE_API_KEY", "")
    services = cpe_load(api_key)

    service_ids = []
    with Pool() as p:
        service_ids = p.map(store_service, services)

    chocolatey()
    debian()
    tag_services(service_ids)


if __name__ == "__main__":
    main()
