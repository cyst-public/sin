import requests
from typing import TypedDict


class Repo(TypedDict):
    description: str
    downloads: int


def get_api_url(url: str) -> str:
    """
    Transform a GitHub URL into the address corresponding to
    its API endpoint.
    """
    path = url.split("github.com/", 1)
    if len(path) < 2:
        return ""
    elements = path[1].split("/")
    if len(elements) < 2:
        return ""
    owner = elements[0]
    repo = elements[1]
    return f"https://api.github.com/repos/{owner}/{repo}"


def fetch(repo_url: str) -> Repo:
    """
    Fetch a GitHub repository and extracts metadata.
    """
    r = requests.get(repo_url + "/releases")
    downloads = 0

    if r.status_code == 200:
        data = r.json()

        for release in data:
            for asset in release.get("assets", []):
                downloads += asset.get("download_count", 0)

    r = requests.get(repo_url)
    if r.status_code == 200:
        data = r.json()

        desc = data.get("description", "")

    return {"description": desc, "downloads": downloads}


def get_repo(url: str) -> Repo:
    """
    Get repository metadata.
    """
    api_path = get_api_url(url)
    return fetch(api_path)
