import requests
from bs4 import BeautifulSoup
from multiprocessing import Pool
from typing import List, TypedDict, Optional
from api import get_services_by_title, update_service
import re


class Package(TypedDict):
    title: str
    downloads: int
    description: str
    ref: str


def get_packages(page: int) -> Optional[List[Package]]:
    """
    Scrape a single page of Chocolatey packages for package metadata.
    """
    url = f"https://community.chocolatey.org/packages"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:110.0) Gecko/20100101 Firefox/110.0"
    }
    params = {
        "sortOrder": "package-download-count",
        "prerelease": False,
        "moderatorQueue": False,
        "moderationStatus": "all-statuses",
        "page": page,
    }

    try:
        html = requests.get(url, headers=headers, params=params)
        bs = BeautifulSoup(html.content, "lxml")
        section = bs.find("section", {"id": "package"})

        for tag in section.findAll("a", {"class": "h5"}):
            tag.find("span").decompose()
        titles = list(
            map(lambda tag: tag.get_text().strip(), section.findAll("a", {"class": "h5"}))
        )

        downloads = list(
            map(
                lambda tag: int(tag.get_text().replace(" Downloads", "").replace(",", "")),
                section.findAll("span", {"class": "badge", "class": "text-primary"}),
            )
        )

        for tag in section.select("li > div > p"):
            link = tag.find("a")
            if link is not None:
                link.decompose()
        descriptions = list(
            map(lambda tag: " ".join(tag.get_text().split()), section.select("li > div > p"))
        )

        refs = [
            "https://community.chocolatey.org" + a["href"]
            for a in section.findAll("a", {"class": "h5"})
        ]

        assert len(titles) == len(downloads) == len(descriptions) == len(refs)

        return [
            {"title": title, "downloads": downloads, "description": desc, "ref": ref}
            for title, downloads, desc, ref in zip(titles, downloads, descriptions, refs)
        ]
    except:
        return None


def update_services(package: Package) -> bool:
    """
    Update all services with the package data, based on the title match.
    """
    package_title = package.get("title", "")

    if not package_title:
        return False

    product_title = re.sub("\(.*?\)|\[.*?\]", "", package_title).strip()
    services = get_services_by_title(product_title)

    if services is None:
        return False

    for service in services:
        if service.get("downloads") and service.get("description"):
            continue

        update_fields = {
            "downloads": package["downloads"],
            "refs": {"source": [package["ref"]]},
            "description": package["description"],
        }

        update_service(service["pk"], update_fields)
    return True


def main():
    """
    Obtain all the packages from Chocolatey and updates the
    corresponding Services in the repository.
    """
    page = 1
    packages = []

    data = get_packages(page)
    while data:
        packages += data
        page += 1
        data = get_packages(page)

        if page % 50 == 0:
            print(f"[CHOCO] fetched {30 * page}")
    print(f"[CHOCO] fetched {len(packages)}")

    with Pool() as p:
        p.map(update_services, packages)


if __name__ == "__main__":
    main()
