from sources.chocolatey import main as chocolatey
from sources.debian import main as debian
from sources.cpe import (
    load as cpe_load,
    update as cpe_update,
)
from sources.github import get_repo as github_get_repo
from sources.scraper import get_description
