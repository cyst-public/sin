import requests
from multiprocessing import Pool
from os import environ
from cpe.cpe2_3 import CPE2_3
from math import inf
from typing import List, Dict, Optional, TypedDict, Any
from datetime import datetime, timedelta
from template import Service

SOURCE_URL = "https://services.nvd.nist.gov/rest/json/cpes/2.0"


class CPERef(TypedDict):
    ref: str
    type: str


class CPETitle(TypedDict):
    title: str
    lang: str


class CPEData(TypedDict, total=False):
    deprecated: bool
    cpeName: str
    cpeNameId: str
    created: datetime
    lastModified: datetime
    titles: List[CPETitle]
    refs: List[CPERef]


class CPEItem(TypedDict, total=False):
    cpe: CPEData


def extract_service_data(cpe_item: CPEItem) -> Optional[Dict[str, Any]]:
    """
    Extract the data from a CPE and populate the Service template with it.
    """
    cpe_data = cpe_item["cpe"]

    try:
        cpe2_3 = CPE2_3(cpe_data["cpeName"])

        en_title = ""
        for title in cpe_data["titles"]:
            if title["lang"] == "en":
                en_title = title["title"]
                break

        cpe = {
            "name": cpe_data["cpeName"],
            "id": cpe_data["cpeNameId"],
            "last_modified": cpe_data["lastModified"],
            "created": cpe_data["created"],
        }

        refs = {}

        for ref in cpe_data.get("refs", []):
            ref_type = ref.get("type", "")

            if not ref_type:
                ref_type = "misc"
            else:
                ref_type = ref_type.lower().replace(" ", "_")

            if ref_type not in refs:
                refs[ref_type] = [ref["ref"]]
            else:
                refs[ref_type].append(ref["ref"])

        version = cpe2_3.get_version()[0]

        service = {
            "cpe": cpe,
            "title": en_title,
            "refs": refs,
            "vendor": cpe2_3.get_vendor()[0],
            "product": cpe2_3.get_product()[0],
            "version": "x" if version == "-" else version,  # replace version wildcard
            "target_sw": cpe2_3.get_target_software()[0],
        }
        return service
    except NotImplementedError:
        return None


def fetch(headers: Dict[str, str], params: Dict[str, str]) -> List[Dict[str, CPEData]]:
    """
    Fetch records from the CPE Dictionary.
    """
    start_index = 0
    total_results = inf
    max_results = float(environ.get("CPE_RECORDS_COUNT", inf))
    payload = params
    products = []

    while (start_index < total_results) and (start_index < max_results):
        payload["startIndex"] = start_index
        r = requests.get(SOURCE_URL, headers=headers, params=payload)

        if r.status_code == 200:
            data = r.json()

            with Pool() as p:
                products += p.map(extract_service_data, data["products"])

            start_index += data["resultsPerPage"]
            total_results = data["totalResults"]
        else:
            print(r.headers.get("message", "Unknown error"))

        print(f"[CPE] fetched {start_index}")
    return products


def update(days: int, api_key: str = "") -> List[Optional[Service]]:
    """
    Get all the CPE Dictionary records that changed in the last N days.
    """
    end = datetime.utcnow()
    start = end - timedelta(days=days)
    headers = {"apiKey": api_key} if api_key else {}
    payload = {
        "lastModStartDate": start.isoformat(),
        "lastModEndDate": end.isoformat(),
        "cpeMatchString": "cpe:2.3:a:*:*:*:*:*:*:*:*:*:*",
    }

    products = fetch(headers, payload)
    return products


def load(api_key: str = "") -> List[Optional[Service]]:
    """
    Get all the CPE Dictionary records.
    """
    headers = {"apiKey": api_key} if api_key else {}
    payload = {"cpeMatchString": "cpe:2.3:a:*:*:*:*:*:*:*:*:*:*"}

    products = fetch(headers, payload)
    return products
