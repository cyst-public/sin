import requests
from bs4 import BeautifulSoup
import re
from typing import Optional


def get_description(url: str) -> Optional[str]:
    """
    Get a description from a site, by looking for tags indicating description
    content or simply providing some information, like headings.
    """
    description = re.compile(".*(desc)")

    try:
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:110.0) Gecko/20100101 Firefox/110.0",
            "Accept-Language": "en",
        }
        html = requests.get(url, headers=headers)
        bs = BeautifulSoup(html.text, "lxml")

        desc_tags = bs.find_all(id=description)

        if not desc_tags:
            desc_tags = bs.find_all(class_=description)

        if not desc_tags:
            return bs.find("h1").get_text().strip()

        return " ".join(
            list(
                map(
                    lambda tag: " ".join(tag.get_text().split() if tag is not None else ""),
                    desc_tags,
                )
            )
        )
    except:
        return None
