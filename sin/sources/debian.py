import requests
from multiprocessing import Pool
from bs4 import BeautifulSoup
from typing import TypedDict, List, Optional, Dict
from api import get_services_by_title, update_service


class Package(TypedDict):
    title: str
    description: str
    refs: Dict[str, List[str]]


def fetch_package_names() -> List[str]:
    """
    Return a list of all the debian package names.
    """
    res = requests.get("https://sources.debian.org/api/list")
    data = res.json()
    return list(map(lambda package: package["name"], data["packages"]))


def fetch_package(name: str) -> Optional[Package]:
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:110.0) Gecko/20100101 Firefox/110.0"
    }

    package_url = f"https://tracker.debian.org/pkg/{name}"

    try:
        html = requests.get(package_url, headers=headers)
        bs = BeautifulSoup(html.content, "lxml")

        description = bs.find("h5").get_text()

        homepage = bs.find("a", {"title": "upstream web homepage"})
        if homepage is not None:
            homepage_link = homepage["href"]

        return {
            "title": name,
            "refs": {"source": [package_url], "product": [homepage_link]},
            "description": description,
        }
    except:
        return None


def update_services(package: Package) -> bool:
    """
    Update all Services with the package data, based on the title match.
    """
    package_title = package.get("title", "")

    if not package_title:
        return False

    services = get_services_by_title(package_title)

    if services is None:
        return True

    for service in services:
        if service.get("downloads") and service.get("description"):
            continue

        update_fields = {"refs": package["refs"], "description": package["description"]}

        update_service(service["pk"], update_fields)
    return True


def process_package(name: str) -> None:
    """
    Fetches the package by the name and matches it to a Service in the registry.
    """
    data = fetch_package(name)

    if data is not None:
        update_services(data)


def main():
    """
    Obtain all Debian packages and update Services in the registry accordingly.
    """
    package_names = fetch_package_names()

    print(f"[DEB] fetched {len(package_names)}")

    with Pool() as p:
        p.map(process_package, package_names)


if __name__ == "__main__":
    main()
